package com.revolut.core.model

import java.util.*

/**
 * Created by Alexandr Lyadinskii on 24/07/2017.
 */

data class CurrencyInEdit(
    val currency: Currency,
    val type: ExchangeType
)
