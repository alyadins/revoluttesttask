package com.revolut.core.model

/**
 * Created by Alexandr Lyadinskii on 20/07/2017.
 */

enum class ExchangeType {
    FROM,
    TO,
    NONE
}
