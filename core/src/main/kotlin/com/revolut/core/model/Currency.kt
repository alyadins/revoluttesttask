package com.revolut.core.model

import java.util.*


/**
 * Created by Alexandr Lyadinskii on 18/07/2017.
 */

object Currencies {

    val GBP = Currency.getInstance("GBP")!!
    val EUR = Currency.getInstance("EUR")!!
    val USD = Currency.getInstance("USD")!!
    val PLN = Currency.getInstance("PLN")!!
    val CHF = Currency.getInstance("CHF")!!

    val ALL = arrayListOf(GBP, EUR, USD, PLN, CHF)

    fun valueOf(name: String): Currency? {
        return ALL.filter { it.currencyCode == name }.firstOrNull()
    }
}
