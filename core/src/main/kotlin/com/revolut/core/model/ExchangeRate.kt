package com.revolut.core.model

import java.math.BigDecimal
import java.util.*

/**
 * Created by Alexandr Lyadinskii on 18/07/2017.
 */
data class ExchangeRate(
    val from: Currency,
    val to: Currency,
    val rate: BigDecimal) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false

        other as ExchangeRate

        return other.from == from && other.to == to
    }

    override fun hashCode(): Int {
        var result = from.hashCode()
        result = 31 * result + to.hashCode()
        return result
    }
}