package com.revolut.core.di.qualifier

import javax.inject.Qualifier

/**
 * Created by Alexandr Lyadinskii on 24/07/2017.
 */

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class NetworkThread