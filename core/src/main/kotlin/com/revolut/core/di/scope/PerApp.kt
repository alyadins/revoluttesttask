package com.revolut.core.di.scope

import javax.inject.Scope

/**
 * Created by Alexandr Lyadinskii on 18/07/2017.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerApp
