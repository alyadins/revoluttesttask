package com.revolut.core.presenter

import java.util.*

/**
 * Created by Alexandr Lyadinskii on 20/07/2017.
 */

interface ExchangePagerPresenter {

    fun onBackClick()

    fun fromCurrencySelected(currency: Currency)

    fun toCurrencySelected(currency: Currency)
}
