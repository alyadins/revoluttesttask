package com.revolut.core.presenter

import com.revolut.core.model.ExchangeType
import java.util.*

/**
 * Created by Alexandr Lyadinskii on 20/07/2017.
 */

interface ExchangePagerItemPresenter {

    var exchangeType: ExchangeType
    var currency: Currency

    fun setInEdit()
    fun onValueChange(value: String)
}
