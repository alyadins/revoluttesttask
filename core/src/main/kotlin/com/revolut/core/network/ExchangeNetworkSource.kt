package com.revolut.core.network

import com.revolut.core.model.ExchangeRate
import io.reactivex.Single
import java.util.*

/**
 * Created by Alexandr Lyadinskii on 18/07/2017.
 */

interface ExchangeNetworkSource {

    fun getExchangeRate(base: Currency, to: List<Currency>): Single<List<ExchangeRate>>
}
