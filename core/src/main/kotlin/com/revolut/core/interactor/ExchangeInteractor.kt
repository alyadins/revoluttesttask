package com.revolut.core.interactor

import java.math.BigDecimal
import java.util.*

/**
 * Created by Alexandr Lyadinskii on 20/07/2017.
 */

interface ExchangeInteractor {

    fun setToCurrency(currency: Currency)
    fun setFromCurrency(currency: Currency)
    fun updateEditableValue(value: BigDecimal)

    fun release()

}
