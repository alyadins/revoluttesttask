package com.revolut.testtask.ui.controller

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import com.revolut.testtask.ui.base.BaseController
import java.util.*

/**
 * Created by Alexandr Lyadinskii on 20/07/2017.
 */

class StubController(args: Bundle? = null) : BaseController(args) {

    val random = Random()

    override fun createView(container: ViewGroup): View {
        val v = View(container.context)
        val params = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        v.layoutParams = params
        v.setBackgroundColor(Color.argb(255, random.nextInt(255), random.nextInt(255), random.nextInt(255)))

        return v
    }
}
