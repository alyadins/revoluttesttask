package com.revolut.testtask.ui.adapter.pager

import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.support.RouterPagerAdapter
import com.revolut.core.model.ExchangeType
import com.revolut.testtask.ui.controller.exchange.ExchangePagerItemController
import java.util.*

/**
 * Created by Alexandr Lyadinskii on 20/07/2017.
 */

class ExchangePagerAdapter(
    val exchangeType: ExchangeType,
    val supportedCurrencies: List<Currency>,
    host: Controller
) : RouterPagerAdapter(host) {
    override fun configureRouter(router: Router, position: Int) {
        if (!router.hasRootController()) {
            val currency = supportedCurrencies[position]
            router.pushController(RouterTransaction.with(ExchangePagerItemController.create(exchangeType, currency)))
        }
    }

    override fun getCount(): Int {
        return supportedCurrencies.size
    }

    fun getCurrencyForPosition(position: Int): Currency {
        return supportedCurrencies[position]
    }
}
