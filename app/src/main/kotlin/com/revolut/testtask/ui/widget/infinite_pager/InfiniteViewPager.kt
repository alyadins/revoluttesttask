package com.revolut.testtask.ui.widget.infinite_pager

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.util.AttributeSet

/**
 * Created by Alexandr Lyadinskii on 20/07/2017.
 */

class InfiniteViewPager
@JvmOverloads constructor(context: Context, attrs: AttributeSet? = null) : ViewPager(context, attrs) {

    override fun setAdapter(adapter: PagerAdapter) {
        super.setAdapter(adapter)
        currentItem = 0
    }

    override fun setCurrentItem(item: Int) {
        setCurrentItem(item, false)
    }

    override fun setCurrentItem(item: Int, smoothScroll: Boolean) {
        var position = item
        if (adapter.count == 0) {
            super.setCurrentItem(position, smoothScroll)
            return
        }
        position = offsetAmount + item % adapter.count
        super.setCurrentItem(position, smoothScroll)
    }


    override fun getCurrentItem(): Int {
        if (adapter.count == 0) {
            return super.getCurrentItem()
        }
        val position = super.getCurrentItem()
        if (adapter is InfinitePagerAdapterWrapper) {
            val infAdapter = adapter as InfinitePagerAdapterWrapper
            return position % infAdapter.realCount
        } else {
            return super.getCurrentItem()
        }
    }


    val offsetAmount: Int
        get() {
            if (adapter.count == 0) {
                return 0
            }
            if (adapter is InfinitePagerAdapterWrapper) {
                val infAdapter = adapter as InfinitePagerAdapterWrapper
                return infAdapter.realCount * PAGE_MULTIPLIER
            } else {
                return 0
            }
        }

    companion object {
        const val PAGE_MULTIPLIER = 1000
    }
}