package com.revolut.testtask.ui.controller

import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.OnClick
import com.bluelinelabs.conductor.RouterTransaction
import com.revolut.testtask.R
import com.revolut.testtask.ui.base.BaseController
import com.revolut.testtask.ui.controller.exchange.ExchangePagerController
import com.revolut.testtask.ui.util.inflate

/**
 * Created by Alexandr Lyadinskii on 17/07/2017.
 */

class HomeController(args: Bundle? = null) : BaseController(args) {

    @BindView(R.id.toolbar) lateinit var toolbar: Toolbar

    override fun createView(container: ViewGroup): View {
        return container.inflate(R.layout.controller_home)
    }

    override fun viewBind(view: View) {
        toolbar.setTitle(R.string.app_name)
    }

    @OnClick(R.id.exchange_screen)
    fun onExchangeScreenButtonClick() {
        router.pushController(RouterTransaction.with(ExchangePagerController.create()))
    }

    companion object {
        fun create() = HomeController()
    }
}
