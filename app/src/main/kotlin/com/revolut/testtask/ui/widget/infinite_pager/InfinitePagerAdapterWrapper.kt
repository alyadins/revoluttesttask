package com.revolut.testtask.ui.widget.infinite_pager

import android.database.DataSetObserver
import android.os.Parcelable
import android.support.v4.view.PagerAdapter
import android.view.View
import android.view.ViewGroup

/**
 * Created by Alexandr Lyadinskii on 20/07/2017.
 */

class InfinitePagerAdapterWrapper(val realAdapter: PagerAdapter) : PagerAdapter() {

    val realCount: Int
        get() = realAdapter.count

    override fun getCount(): Int {
        if (realCount == 0) {
            return 0
        }
        return Integer.MAX_VALUE
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val virtualPosition = position % realCount
        return realAdapter.instantiateItem(container, virtualPosition)
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val virtualPosition = position % realCount
        realAdapter.destroyItem(container, virtualPosition, `object`)
    }

    override fun finishUpdate(container: ViewGroup) {
        realAdapter.finishUpdate(container)
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return realAdapter.isViewFromObject(view, `object`)
    }

    override fun restoreState(bundle: Parcelable?, classLoader: ClassLoader?) {
        realAdapter.restoreState(bundle, classLoader)
    }

    override fun saveState(): Parcelable? {
        return realAdapter.saveState()
    }

    override fun startUpdate(container: ViewGroup) {
        realAdapter.startUpdate(container)
    }

    override fun getPageTitle(position: Int): CharSequence {
        val virtualPosition = position % realCount
        return realAdapter.getPageTitle(virtualPosition)
    }

    override fun getPageWidth(position: Int): Float {
        return realAdapter.getPageWidth(position)
    }

    override fun setPrimaryItem(container: ViewGroup, position: Int, `object`: Any) {
        realAdapter.setPrimaryItem(container, position, `object`)
    }

    override fun unregisterDataSetObserver(observer: DataSetObserver) {
        realAdapter.unregisterDataSetObserver(observer)
    }

    override fun registerDataSetObserver(observer: DataSetObserver) {
        realAdapter.registerDataSetObserver(observer)
    }

    override fun notifyDataSetChanged() {
        realAdapter.notifyDataSetChanged()
    }

    override fun getItemPosition(`object`: Any): Int {
        return realAdapter.getItemPosition(`object`)
    }

    companion object {

        fun wrap(adapter: PagerAdapter): InfinitePagerAdapterWrapper {
            return InfinitePagerAdapterWrapper(adapter)
        }
    }
}