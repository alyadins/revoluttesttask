package com.revolut.testtask.ui.util

import android.text.Editable
import android.text.TextWatcher

/**
 * Created by Alexandr Lyadinskii on 01/08/2017.
 */

abstract class SimpleTextWatcher : TextWatcher {
    override fun afterTextChanged(s: Editable) {
    }

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
    }

}
