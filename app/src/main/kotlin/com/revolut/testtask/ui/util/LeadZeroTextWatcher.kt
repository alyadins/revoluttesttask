package com.revolut.testtask.ui.util

import android.text.Editable

/**
 * Created by Alexandr Lyadinskii on 20/07/2017.
 */
class LeadZeroTextWatcher : SimpleTextWatcher() {
    val regEx = Regex("^0+(?!$)")
    var ignore = false
    var previous: String? = null

    override fun afterTextChanged(s: Editable) {

        if (ignore) {
            return
        }

        var text = s.toString()
        ignore = true
        if (text.startsWith(ZERO) && text != ZERO && !text.startsWith(ZERO_WITH_DOT)) {
            text = s.replaceFirst(regEx, "")
            s.clear()
            s.append(text)
        } else if (previous == ZERO && text.endsWith(ZERO) && text.length > 1) {
            s.delete(text.length - 1, text.length)
        }


        if (text.isEmpty()) {
            s.replace(0, 0, ZERO)
        }

        ignore = false
        previous = s.toString()

    }

    companion object {
        const val ZERO = "0"
        const val ZERO_WITH_DOT = "0."
    }
}
