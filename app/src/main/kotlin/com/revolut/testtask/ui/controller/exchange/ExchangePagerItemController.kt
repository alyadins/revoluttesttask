package com.revolut.testtask.ui.controller.exchange

import android.os.Bundle
import android.text.Editable
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.revolut.core.model.ExchangeType
import com.revolut.testtask.R
import com.revolut.testtask.di.exchange.ExchangePagerItemInjector
import com.revolut.testtask.presenter.ExchangePagerItemPresenterImpl
import com.revolut.testtask.ui.base.BaseController
import com.revolut.testtask.ui.util.LeadZeroTextWatcher
import com.revolut.testtask.ui.util.SimpleTextWatcher
import com.revolut.testtask.ui.util.inflate
import com.revolut.testtask.view.ExchangePagerItemView
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*

/**
 * Created by Alexandr Lyadinskii on 20/07/2017.
 */

class ExchangePagerItemController(args: Bundle? = null) : BaseController(args), ExchangePagerItemView {

    @BindView(R.id.currency_name) lateinit var currencyName: TextView
    @BindView(R.id.amount_edit) lateinit var amountEdit: TextView

    @InjectPresenter
    lateinit var itemPresenter: ExchangePagerItemPresenterImpl

    override fun createView(container: ViewGroup): View {
        return container.inflate(R.layout.controller_exchange_pager_item)
    }


    override fun viewBind(view: View) {
        super.viewBind(view)
        val currency = args.getSerializable(ARG_CURRENCY) as Currency
        currencyName.text = currency.currencyCode

        amountEdit.addTextChangedListener(LeadZeroTextWatcher())
        amountEdit.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                itemPresenter.setInEdit()
            }
        }
        amountEdit.addTextChangedListener(object : SimpleTextWatcher() {
            override fun afterTextChanged(s: Editable) {
                if (isAttached) {
                    itemPresenter.onValueChange(s.toString())
                }
            }
        })
    }

    override fun updateRate(value: BigDecimal) {
        val scaledValue = value.setScale(2, RoundingMode.HALF_UP)
        if (scaledValue.compareTo(BigDecimal.ZERO) != 0) {
            amountEdit.text = scaledValue.toPlainString()
        } else {
            amountEdit.text = "0"
        }
    }

    override fun inject() {
        val injector = findInjector<ExchangePagerItemInjector>()
            ?: throw IllegalStateException("not found injector for this controller")

        injector.inject(itemPresenter)
        val currency = args.getSerializable(ARG_CURRENCY) as Currency
        val exchangeType = args.getSerializable(ARG_EXCHANGE_TYPE) as ExchangeType

        itemPresenter.currency = currency
        itemPresenter.exchangeType = exchangeType
    }

    override fun onSaveViewState(view: View, outState: Bundle) {
        val input = amountEdit.text.toString()
        outState.putString(STATE_INPUT, input)
    }

    override fun onRestoreViewState(view: View, savedViewState: Bundle) {
        val input = savedViewState.getString(STATE_INPUT)
        amountEdit.text = input
    }

    override fun eraseInput() {
        amountEdit.text = null
    }

    override fun requestFocus() {
        amountEdit.requestFocus()
    }

    companion object {
        const val ARG_CURRENCY = "currency"
        const val ARG_EXCHANGE_TYPE = "exchange_type"

        const val STATE_INPUT = "input"

        fun create(exchangeType: ExchangeType, currency: Currency): ExchangePagerItemController {
            val args = Bundle()
            args.putSerializable(ARG_CURRENCY, currency)
            args.putSerializable(ARG_EXCHANGE_TYPE, exchangeType)
            return ExchangePagerItemController(args)
        }
    }
}
