package com.revolut.testtask.ui.base

import android.os.Bundle
import android.support.annotation.CallSuper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.ButterKnife
import butterknife.Unbinder
import com.arellomobile.mvp.MvpDelegate
import com.bluelinelabs.conductor.Controller
import com.revolut.core.di.RevolutComponent
import com.revolut.testtask.di.AppComponent
import com.revolut.testtask.di.ComponentProvider

/**
 * Created by Alexandr Lyadinskii on 17/07/2017.
 */

abstract class BaseController(args: Bundle? = null) : Controller(args), ComponentProvider {


    var unbinder: Unbinder? = null
    val mvpDelegate: MvpDelegate<BaseController> by lazy {
        return@lazy MvpDelegate(this)
    }

    override var component: RevolutComponent? = appComponent()

    init {
        mvpDelegate.onCreate()
    }

    final override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        val v = createView(container)
        unbinder = ButterKnife.bind(this, v)
        viewBind(v)
        return v
    }


    final override fun onAttach(view: View) {
        super.onAttach(view)
        inject()
        viewAttach(view)
        mvpDelegate.onAttach()
    }

    final override fun onDetach(view: View) {
        super.onDetach(view)
        viewDetach(view)
        mvpDelegate.onDetach()
    }


    open fun inject() {

    }

    @CallSuper
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mvpDelegate.onSaveInstanceState(outState)
    }

    @CallSuper
    final override fun onDestroyView(view: View) {
        mvpDelegate.onDestroyView()
        viewDestroy(view)
        unbinder?.unbind()
        super.onDestroyView(view)
    }

    override fun onDestroy() {
        mvpDelegate.onDestroy()
        super.onDestroy()
    }

    fun appComponent(): AppComponent? {
        activity?.let {
            if (it is BaseActivity) {
                return it.appComponent()
            }
        }

        return null
    }

    protected inline fun <reified T> findInjector(): T? {
        var controller: Controller? = this

        while (controller != null) {
            if (controller is ComponentProvider) {
                val component = controller.component
                if (component is T) {
                    return component
                }
            }

            controller = controller.parentController
        }

        activity?.let {
            if (it is ComponentProvider) {
                val component = it.component
                if (component is T) {
                    return component
                }
            }
        }

        return null
    }


    open fun viewBind(view: View) {

    }

    open fun viewAttach(view: View) {

    }

    open fun viewDetach(view: View) {
    }

    open fun viewDestroy(view: View) {

    }

    abstract fun createView(container: ViewGroup): View
}
