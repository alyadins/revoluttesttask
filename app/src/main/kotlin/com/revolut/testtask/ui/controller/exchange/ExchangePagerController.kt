package com.revolut.testtask.ui.controller.exchange

import android.os.Bundle
import android.support.v4.view.ViewPager
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.OnClick
import com.arellomobile.mvp.presenter.InjectPresenter
import com.rd.PageIndicatorView
import com.revolut.core.di.RevolutComponent
import com.revolut.core.model.Currencies
import com.revolut.core.model.ExchangeType
import com.revolut.testtask.R
import com.revolut.testtask.di.ComponentProvider
import com.revolut.testtask.di.exchange.ExchangeInjector
import com.revolut.testtask.di.exchange.ExchangeModule
import com.revolut.testtask.presenter.ExchangePagerPresenterImpl
import com.revolut.testtask.ui.adapter.pager.ExchangePagerAdapter
import com.revolut.testtask.ui.base.BaseController
import com.revolut.testtask.ui.util.inflate
import com.revolut.testtask.ui.widget.infinite_pager.InfinitePagerAdapterWrapper
import com.revolut.testtask.ui.widget.infinite_pager.InfiniteViewPager
import com.revolut.testtask.view.ExchangePagerView
import java.util.*

/**
 * Created by Alexandr Lyadinskii on 17/07/2017.
 */

class ExchangePagerController(args: Bundle? = null) : BaseController(args), ExchangePagerView, ComponentProvider {


    @BindView(R.id.from_pager) lateinit var fromPager: InfiniteViewPager
    @BindView(R.id.from_pager_indicator) lateinit var fromPagerIndicator: PageIndicatorView
    @BindView(R.id.to_pager) lateinit var toPager: InfiniteViewPager
    @BindView(R.id.to_pager_indicator) lateinit var toPagerIndicator: PageIndicatorView

    @InjectPresenter
    lateinit var pagerPresenter: ExchangePagerPresenterImpl //moxy not support interface injection yet

    override var component: RevolutComponent? = exchangeComponent()
        get() {
            if (field == null) {
                field = appComponent()?.plusExchangeComponent(ExchangeModule())
            }

            return field
        }

    override fun createView(container: ViewGroup): View {
        return container.inflate(R.layout.controller_exchange)
    }

    override fun viewBind(view: View) {
        super.viewBind(view)

        //all currencies used for test task only. In real app we must filter (or load) primary currencies in presenter
        val fromAdapter = ExchangePagerAdapter(ExchangeType.FROM, Currencies.ALL, host = this)
        val fromWrappedAdapter = InfinitePagerAdapterWrapper.wrap(fromAdapter)

        val toAdapter = ExchangePagerAdapter(ExchangeType.TO, Currencies.ALL, host = this)
        val toWrappedAdapter = InfinitePagerAdapterWrapper.wrap(toAdapter)

        fromPager.adapter = fromWrappedAdapter
        toPager.adapter = toWrappedAdapter

        fromPagerIndicator.count = fromAdapter.count
        toPagerIndicator.count = toAdapter.count
    }

    override fun viewAttach(view: View) {
        fromPager.addOnPageChangeListener(fromPagerChangeListener)
        toPager.addOnPageChangeListener(toPagerChangeListener)
    }

    override fun viewDetach(view: View) {
        fromPager.removeOnPageChangeListener(fromPagerChangeListener)
        toPager.removeOnPageChangeListener(toPagerChangeListener)
    }

    override fun inject() {
        findInjector<ExchangeInjector>()?.inject(pagerPresenter)
    }

    override fun close() {
        router.popController(this)
    }

    override fun setCurrentCurrencies(from: Currency, to: Currency) {
        fromPager.currentItem = Currencies.ALL.indexOf(from)
        toPager.currentItem = Currencies.ALL.indexOf(to)
    }

    @OnClick(R.id.back_button)
    fun onBackButtonClick() {
        pagerPresenter.onBackClick()
    }

    private fun exchangeComponent(): RevolutComponent? {
        if (component == null) {
            component = appComponent()?.plusExchangeComponent(ExchangeModule())
        }

        return component
    }

    private val fromPagerChangeListener = object : ViewPager.SimpleOnPageChangeListener() {
        override fun onPageSelected(position: Int) {
            val realPosition = fromPager.currentItem
            val adapter = getExchangeAdapter(fromPager)
            fromPagerIndicator.selection = realPosition
            pagerPresenter.fromCurrencySelected(adapter.getCurrencyForPosition(realPosition))
        }
    }

    private val toPagerChangeListener = object : ViewPager.SimpleOnPageChangeListener() {
        override fun onPageSelected(position: Int) {
            val realPosition = toPager.currentItem
            val adapter = getExchangeAdapter(toPager)
            toPagerIndicator.selection = realPosition
            pagerPresenter.toCurrencySelected(adapter.getCurrencyForPosition(realPosition))
        }
    }

    private fun getExchangeAdapter(pager: ViewPager): ExchangePagerAdapter {
        val adapter = pager.adapter as InfinitePagerAdapterWrapper
        return adapter.realAdapter as ExchangePagerAdapter
    }

    companion object {
        fun create() = ExchangePagerController()
    }
}
