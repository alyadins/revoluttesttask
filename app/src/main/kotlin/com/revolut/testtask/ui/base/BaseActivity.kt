package com.revolut.testtask.ui.base

import android.support.v7.app.AppCompatActivity
import com.revolut.core.di.RevolutComponent
import com.revolut.testtask.RevolutApp
import com.revolut.testtask.di.AppComponent
import com.revolut.testtask.di.ComponentProvider

/**
 * Created by Alexandr Lyadinskii on 17/07/2017.
 */

open class BaseActivity : AppCompatActivity(), ComponentProvider {
    override var component: RevolutComponent? = null
        get() = appComponent()

    fun appComponent(): AppComponent {
        return app().appComponent
    }

    private fun app(): RevolutApp {
        return applicationContext as RevolutApp
    }
}
