package com.revolut.testtask

import android.app.Application
import com.revolut.testtask.di.AppComponent
import com.revolut.testtask.di.AppModule
import com.revolut.testtask.di.DaggerAppComponent
import timber.log.Timber

/**
 * Created by Alexandr Lyadinskii on 18/07/2017.
 */


class RevolutApp : Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()

        initAppComponent()
        initTimber()
    }

    private fun initTimber() {
        Timber.plant(Timber.DebugTree())
    }

    private fun initAppComponent() {
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
    }
}