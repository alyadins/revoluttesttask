package com.revolut.testtask.model

import com.revolut.core.model.CurrencyInEdit
import com.revolut.core.model.ExchangeRate
import com.revolut.testtask.di.scope.ExchangeScope
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import java.math.BigDecimal
import java.util.*
import javax.inject.Inject

/**
 * Created by Alexandr Lyadinskii on 01/08/2017.
 */

@ExchangeScope
class ExchangeScreenState @Inject constructor() {

    val fromCurrencySubject: Subject<Currency> = BehaviorSubject.create()
    val toCurrencySubject: Subject<Currency> = BehaviorSubject.create()
    val ratesSubject: Subject<List<ExchangeRate>> = BehaviorSubject.create()
    val editableValueSubject: Subject<BigDecimal> = BehaviorSubject.create()
    var inEdit: CurrencyInEdit? = null

    fun release() {
        fromCurrencySubject.onComplete()
        toCurrencySubject.onComplete()
        ratesSubject.onComplete()
        editableValueSubject.onComplete()
    }
}
