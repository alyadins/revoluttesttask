package com.revolut.testtask.view

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.revolut.testtask.view.base.MoxyView
import java.math.BigDecimal

/**
 * Created by Alexandr Lyadinskii on 20/07/2017.
 */

interface ExchangePagerItemView : MoxyView {

    @StateStrategyType(SingleStateStrategy::class)
    fun eraseInput()

    @StateStrategyType(SingleStateStrategy::class)
    fun requestFocus()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun updateRate(value: BigDecimal)
}
