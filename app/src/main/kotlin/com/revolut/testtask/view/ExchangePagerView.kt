package com.revolut.testtask.view

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.revolut.testtask.view.base.MoxyView
import java.util.*

/**
 * Created by Alexandr Lyadinskii on 20/07/2017.
 */

@StateStrategyType(AddToEndSingleStrategy::class)
interface ExchangePagerView : MoxyView {

    @StateStrategyType(SingleStateStrategy::class)
    fun close()

    @StateStrategyType(SingleStateStrategy::class)
    fun setCurrentCurrencies(from: Currency, to: Currency)
}
