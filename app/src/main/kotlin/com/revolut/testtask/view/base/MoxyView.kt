package com.revolut.testtask.view.base

import com.arellomobile.mvp.MvpView

/**
 * Created by Alexandr Lyadinskii on 20/07/2017.
 */

interface MoxyView : MvpView
