package com.revolut.testtask.util

import io.reactivex.disposables.Disposable

/**
 * Created by Alexandr Lyadinskii on 20/07/2017.
 */

fun Disposable.safeDispose() {
    if (!isDisposed) {
        dispose()
    }
}
