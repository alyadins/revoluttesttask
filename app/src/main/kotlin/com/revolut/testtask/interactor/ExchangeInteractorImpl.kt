package com.revolut.testtask.interactor

import com.revolut.core.interactor.ExchangeInteractor
import com.revolut.core.model.ExchangeRate
import com.revolut.core.network.ExchangeNetworkSource
import com.revolut.testtask.model.ExchangeScreenState
import com.revolut.testtask.util.safeDispose
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.Observables
import timber.log.Timber
import java.math.BigDecimal
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by Alexandr Lyadinskii on 20/07/2017.
 */

class ExchangeInteractorImpl(
    val exchangeNetworkSource: ExchangeNetworkSource,
    val networkScheduler: Scheduler,
    val state: ExchangeScreenState
) : ExchangeInteractor {

    private var pollingDisposable: Disposable? = null

    override fun setToCurrency(currency: Currency) {
        state.toCurrencySubject.onNext(currency)
        restartPolling()
    }

    override fun setFromCurrency(currency: Currency) {
        state.fromCurrencySubject.onNext(currency)
        restartPolling()
    }

    override fun updateEditableValue(value: BigDecimal) {
        state.editableValueSubject.onNext(value)
    }

    override fun release() {
        state.release()
        stopPolling()
    }

    private fun startPolling() {
        pollingDisposable = Observables.zip(state.fromCurrencySubject, state.toCurrencySubject)
            .flatMap({
                val from = it.first
                val to = it.second
                return@flatMap Observable.interval(0L, POLL_INTERVAL_SECONDS, TimeUnit.SECONDS, networkScheduler)
                    .flatMap { exchangeNetworkSource.getExchangeRate(from, arrayListOf(to)).toObservable() }
            })
            .subscribe(this::sendRates, Timber::e)
    }

    private fun stopPolling() {
        pollingDisposable?.safeDispose()
    }

    private fun restartPolling() {
        stopPolling()
        startPolling()
    }

    private fun sendRates(rates: List<ExchangeRate>) {
        state.ratesSubject.onNext(rates)
    }

    companion object {
        const val POLL_INTERVAL_SECONDS = 5L
    }
}
