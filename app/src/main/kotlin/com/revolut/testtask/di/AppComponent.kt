package com.revolut.testtask.di

import com.revolut.core.di.RevolutComponent
import com.revolut.core.di.scope.PerApp
import com.revolut.network.di.ApiModule
import com.revolut.testtask.di.exchange.ExchangeComponent
import com.revolut.testtask.di.exchange.ExchangeModule
import com.revolut.testtask.di.source.NetworkSourceModule
import dagger.Component

/**
 * Created by Alexandr Lyadinskii on 19/07/2017.
 */

@PerApp
@Component(modules = arrayOf(
    AppModule::class,
    ApiModule::class,
    NetworkSourceModule::class,
    RxModule::class))
interface AppComponent : RevolutComponent {

    fun plusExchangeComponent(exchangeModule: ExchangeModule): ExchangeComponent
}
