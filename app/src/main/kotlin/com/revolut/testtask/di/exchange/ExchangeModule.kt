package com.revolut.testtask.di.exchange

import com.revolut.core.di.qualifier.NetworkThread
import com.revolut.core.interactor.ExchangeInteractor
import com.revolut.core.network.ExchangeNetworkSource
import com.revolut.testtask.di.scope.ExchangeScope
import com.revolut.testtask.interactor.ExchangeInteractorImpl
import com.revolut.testtask.model.ExchangeScreenState
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler

/**
 * Created by Alexandr Lyadinskii on 20/07/2017.
 */

@Module
class ExchangeModule {

    @ExchangeScope
    @Provides
    fun provideExchangeInteractor(
        exchangeNetworkSource: ExchangeNetworkSource,
        @NetworkThread networkScheduler: Scheduler,
        exchangeScreenState: ExchangeScreenState
    ): ExchangeInteractor {
        return ExchangeInteractorImpl(exchangeNetworkSource, networkScheduler, exchangeScreenState)
    }

}
