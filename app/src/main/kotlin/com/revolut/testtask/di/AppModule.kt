package com.revolut.testtask.di

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides

/**
 * Created by Alexandr Lyadinskii on 18/07/2017.
 */
@Module
class AppModule(val application: Application) {

    @Provides
    fun provideContext(): Context {
        return application.applicationContext
    }
}
