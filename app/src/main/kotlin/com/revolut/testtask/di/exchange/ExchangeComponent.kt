package com.revolut.testtask.di.exchange

import com.revolut.core.di.RevolutComponent
import com.revolut.testtask.di.scope.ExchangeScope
import dagger.Subcomponent

/**
 * Created by Alexandr Lyadinskii on 20/07/2017.
 */

@ExchangeScope
@Subcomponent(modules = arrayOf(ExchangeModule::class))
interface ExchangeComponent : RevolutComponent, ExchangeInjector, ExchangePagerItemInjector
