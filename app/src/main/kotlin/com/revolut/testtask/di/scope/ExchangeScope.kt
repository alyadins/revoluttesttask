package com.revolut.testtask.di.scope

import javax.inject.Scope

/**
 * Created by Alexandr Lyadinskii on 20/07/2017.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ExchangeScope
