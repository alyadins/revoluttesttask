package com.revolut.testtask.di.source

import com.revolut.core.network.ExchangeNetworkSource
import com.revolut.network.service.ExchangeService
import com.revolut.network.source.ExchangeNetworkSourceImpl
import dagger.Module
import dagger.Provides

/**
 * Created by Alexandr Lyadinskii on 18/07/2017.
 */
@Module
class NetworkSourceModule {

    @Provides
    fun provideExchangeNetworkSource(exchangeService: ExchangeService): ExchangeNetworkSource {
        return ExchangeNetworkSourceImpl(exchangeService)
    }
}
