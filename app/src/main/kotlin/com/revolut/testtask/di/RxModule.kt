package com.revolut.testtask.di

import android.support.annotation.MainThread
import com.revolut.core.di.qualifier.NetworkThread
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Alexandr Lyadinskii on 24/07/2017.
 */

@Module
class RxModule {

    @Provides
    @MainThread
    fun provideMainThreadScheduler(): Scheduler {
        return AndroidSchedulers.mainThread()
    }

    @Provides
    @NetworkThread
    fun provideNetworkThreadScheduler(): Scheduler {
        return Schedulers.io()
    }
}
