package com.revolut.testtask.di

import com.revolut.core.di.RevolutComponent

/**
 * Created by Alexandr Lyadinskii on 24/07/2017.
 */

interface ComponentProvider {

    var component: RevolutComponent?
}
