package com.revolut.testtask.di.exchange

import com.revolut.testtask.presenter.ExchangePagerPresenterImpl

/**
 * Created by Alexandr Lyadinskii on 24/07/2017.
 */

interface ExchangeInjector {
    fun inject(presenter: ExchangePagerPresenterImpl)
}
