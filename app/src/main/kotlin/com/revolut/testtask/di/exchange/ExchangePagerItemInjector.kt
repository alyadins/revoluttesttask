package com.revolut.testtask.di.exchange

import com.revolut.testtask.presenter.ExchangePagerItemPresenterImpl

/**
 * Created by Alexandr Lyadinskii on 24/07/2017.
 */

interface ExchangePagerItemInjector {
    fun inject(presenter: ExchangePagerItemPresenterImpl)
}
