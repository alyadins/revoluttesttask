package com.revolut.testtask.presenter

import com.arellomobile.mvp.InjectViewState
import com.revolut.core.interactor.ExchangeInteractor
import com.revolut.core.model.Currencies
import com.revolut.core.presenter.ExchangePagerPresenter
import com.revolut.testtask.presenter.base.MoxyPresenter
import com.revolut.testtask.view.ExchangePagerView
import java.util.*
import javax.inject.Inject

/**
 * Created by Alexandr Lyadinskii on 20/07/2017.
 */
@InjectViewState
class ExchangePagerPresenterImpl : MoxyPresenter<ExchangePagerView>(), ExchangePagerPresenter {

    @Inject lateinit var exchangeInteractor: ExchangeInteractor

    override fun onFirstViewAttach() {
        viewState.setCurrentCurrencies(from = Currencies.EUR, to = Currencies.USD)
    }

    override fun onBackClick() {
        viewState.close()
    }

    override fun toCurrencySelected(currency: Currency) {
        exchangeInteractor.setToCurrency(currency)
    }

    override fun fromCurrencySelected(currency: Currency) {
        exchangeInteractor.setFromCurrency(currency)
    }

    override fun onDestroy() {
        exchangeInteractor.release()
        super.onDestroy()
    }
}
