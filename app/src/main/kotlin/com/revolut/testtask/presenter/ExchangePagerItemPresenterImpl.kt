package com.revolut.testtask.presenter

import com.arellomobile.mvp.InjectViewState
import com.revolut.core.di.qualifier.MainThread
import com.revolut.core.interactor.ExchangeInteractor
import com.revolut.core.model.CurrencyInEdit
import com.revolut.core.model.ExchangeType
import com.revolut.core.model.ExchangeType.FROM
import com.revolut.core.model.ExchangeType.TO
import com.revolut.core.presenter.ExchangePagerItemPresenter
import com.revolut.testtask.model.ExchangeScreenState
import com.revolut.testtask.presenter.base.MoxyPresenter
import com.revolut.testtask.view.ExchangePagerItemView
import io.reactivex.Scheduler
import io.reactivex.rxkotlin.Observables
import io.reactivex.rxkotlin.plusAssign
import timber.log.Timber
import java.math.BigDecimal
import java.util.*
import javax.inject.Inject

/**
 * Created by Alexandr Lyadinskii on 20/07/2017.
 */
@InjectViewState
class ExchangePagerItemPresenterImpl : MoxyPresenter<ExchangePagerItemView>(), ExchangePagerItemPresenter {

    @Inject lateinit var exchangeInteractor: ExchangeInteractor
    @Inject @MainThread lateinit var mainThreadScheduler: Scheduler
    @Inject lateinit var state: ExchangeScreenState

    override lateinit var exchangeType: ExchangeType
    override lateinit var currency: Currency

    override fun onFirstViewAttach() {

        val currentTypeObservable = when (exchangeType) {
            FROM -> state.fromCurrencySubject
            TO -> state.toCurrencySubject
            else -> throw IllegalStateException("exchangeType $exchangeType not supported")
        }

        untilDestroy += currentTypeObservable
            .subscribe({ viewState.eraseInput() })

        val exchangeRateObservable = state.ratesSubject
            .flatMapIterable { it }
            .filter {
                return@filter when (exchangeType) {
                    ExchangeType.FROM -> it.from == currency
                    ExchangeType.TO -> it.to == currency
                    else -> throw IllegalStateException("not supported")
                }
            }

        val valueObservable = state.editableValueSubject

        untilDestroy +=
            Observables.combineLatest(exchangeRateObservable, valueObservable)
                .filter {
                    val inEdit = state.inEdit
                    if (inEdit != null) {
                        return@filter inEdit.type != exchangeType
                    } else {
                        return@filter false
                    }
                }
                .observeOn(mainThreadScheduler)
                .subscribe({
                    val rate = it.first.rate
                    val value = it.second
                    viewState.updateRate(rate * value)
                }, Timber::e)
    }

    override fun setInEdit() {
        state.inEdit = CurrencyInEdit(currency, exchangeType)
    }

    override fun onValueChange(value: String) {
        state.inEdit?.let {
            if (it.currency == currency && it.type == exchangeType) {
                try {
                    val decimalValue = BigDecimal(value)
                    exchangeInteractor.updateEditableValue(decimalValue)
                } catch (e: NumberFormatException) {
                    Timber.w(e.message)
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}
