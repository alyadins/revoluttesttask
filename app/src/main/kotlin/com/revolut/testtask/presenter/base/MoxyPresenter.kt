package com.revolut.testtask.presenter.base

import com.arellomobile.mvp.MvpPresenter
import com.revolut.testtask.util.safeDispose
import com.revolut.testtask.view.base.MoxyView
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by Alexandr Lyadinskii on 20/07/2017.
 */

open class MoxyPresenter<View : MoxyView> : MvpPresenter<View>() {

    val untilDestroy: CompositeDisposable = CompositeDisposable()

    override fun onDestroy() {
        untilDestroy.safeDispose()
        super.onDestroy()
    }
}
