package com.revolut.network.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.revolut.core.di.scope.PerApp
import com.revolut.network.Urls
import com.revolut.network.di.qualifier.BaseUrl
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit

/**
 * Created by Alexandr Lyadinskii on 18/07/2017.
 */

@Module
class NetworkModule {

    @Provides @PerApp @BaseUrl
    fun provideBaseUrl(): String {
        return Urls.BASE_URL
    }

    @Provides @PerApp
    fun provideGson(): Gson {
        return GsonBuilder()
            .create()
    }

    @Provides
    fun provideLoggingIterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor(object : HttpLoggingInterceptor.Logger {
            private val sb = StringBuffer()

            override fun log(message: String) {
                if (message.contains("<--") || message.contains("-->") && sb.isNotEmpty()) {
                    sb.append(message)
                    Timber.i(sb.toString())
                    sb.setLength(0)
                    sb.append("OkHttp: ")
                } else {
                    sb.append(message).append("\n")
                }
            }
        })
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return interceptor
    }

    @Provides @PerApp
    fun provideOkHttpClient(
        httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient {

        val builder = OkHttpClient.Builder()
        builder
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
        builder.addInterceptor(httpLoggingInterceptor)
        return builder.build()
    }

    @Provides @PerApp
    fun provideRetrofit(@BaseUrl baseUrl: String, okHttpClient: OkHttpClient, gson: Gson): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(okHttpClient)
            .build()
    }
}


