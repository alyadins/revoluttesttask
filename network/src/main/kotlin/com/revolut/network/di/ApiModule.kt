package com.revolut.network.di

import com.revolut.network.service.ExchangeService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

/**
 * Created by Alexandr Lyadinskii on 18/07/2017.
 */
@Module(includes = arrayOf(NetworkModule::class))
class ApiModule {

    @Provides
    fun provideExchangeApiService(retrofit: Retrofit): ExchangeService {
        return retrofit.create(ExchangeService::class.java)
    }
}
