package com.revolut.network.di.qualifier

import javax.inject.Qualifier

/**
 * Created by Alexandr Lyadinskii on 18/07/2017.
 */
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class BaseUrl