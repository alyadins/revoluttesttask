package com.revolut.network.source

import com.revolut.core.model.ExchangeRate
import com.revolut.core.network.ExchangeNetworkSource
import com.revolut.network.model.ratesFromDto
import com.revolut.network.service.ExchangeService
import io.reactivex.Single
import java.util.*

/**
 * Created by Alexandr Lyadinskii on 18/07/2017.
 */
class ExchangeNetworkSourceImpl(val exchangeApi: ExchangeService) : ExchangeNetworkSource {

    override fun getExchangeRate(base: Currency, to: List<Currency>): Single<List<ExchangeRate>> {
        val supportedString = to.foldIndexed("", operation = { index, previous, currency ->
            val sb = StringBuilder()
            sb.append(previous)
            sb.append(currency.currencyCode)

            if (index != to.size - 1) {
                sb.append(",")
            }

            return@foldIndexed sb.toString()
        })

        return exchangeApi.exchangeRates(base = base.currencyCode, to = supportedString)
            .map { it.ratesFromDto() }
    }
}