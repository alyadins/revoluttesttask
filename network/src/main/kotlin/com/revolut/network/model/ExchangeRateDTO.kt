package com.revolut.network.model

import com.revolut.core.model.Currencies
import com.revolut.core.model.ExchangeRate
import java.math.BigDecimal

/**
 * Created by Alexandr Lyadinskii on 18/07/2017.
 */

data class ExchangeRateDTO(
    val base: String,
    val date: String,
    val rates: Map<String, Double>?)

fun ExchangeRateDTO.ratesFromDto(): List<ExchangeRate> {
    val from = Currencies.valueOf(base) ?: throw IllegalStateException("currency with name $base not found")

    val result: MutableList<ExchangeRate> = mutableListOf()

    //map destructing not work on 7 jre devices (check it in newer kotlin versions)
    rates?.forEach { name, rateD ->

        val to = Currencies.valueOf(name) ?: throw IllegalStateException("currency with name $name not found")
        val rate = BigDecimal(rateD)

        result.add(ExchangeRate(from, to, rate))
    }

    return result
}