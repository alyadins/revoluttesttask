package com.revolut.network.service

import com.revolut.network.model.ExchangeRateDTO
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Alexandr Lyadinskii on 18/07/2017.
 */

interface ExchangeService {

    @GET("latest")
    fun exchangeRates(@Query("base") base: String, @Query("symbols") to: String): Single<ExchangeRateDTO>
}
