package com.revolut.network

/**
 * Created by Alexandr Lyadinskii on 18/07/2017.
 */
object Urls {
    const val BASE_URL = "http://api.fixer.io"
}